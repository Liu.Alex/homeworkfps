using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    //message displayed to player when looking at an interactable
    public string prmptMessage;

    //this function will be called by player.
    public void BaseInteract()
    {
        Interact();
    }
    protected virtual void Interact()
    {
        //This is a temp function to be Override by Our subclasses

    }


}
